<!doctype html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Ar.js ~ demo</title>
        <!--
        <link rel="icon" type="image/png" href="img/logo8.png" />
        <script src="js/happywalks.js"></script>
        <link rel="stylesheet" href="css/happywalks.css">
        -->
        <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.5.0/aframe/examples/vendor/aframe/build/aframe.min.js"></script>
        <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.5.0/aframe/build/aframe-ar.js"></script>
    </head>
    <body style='margin : 0px; overflow: hidden;'>
        <a-scene embedded arjs='trackingMethod: best;'>
            <a-anchor hit-testing-enabled='true'>
            <a-box position='0 0.5 0' material='opacity: 0.5;'></a-box>
            </a-anchor>
            <a-camera-static/>
        </a-scene>
    </body>
</html>